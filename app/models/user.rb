class User < ActiveRecord::Base
  attr_accessible :name, :address, :age, :avatar
  
  has_attached_file :avatar, {:styles => {:small => "50*50>", :thumb => "50*50>"}, :default_url => "/assets/avatar.png"}
  
  validates_attachment_content_type :avatar, :content_type => ['image/jpeg','image/jpg', 'image/png', 'image/gif','image/bmp', 'image/x-png', 'image/pjpeg'] 


# or you can do like this also 
if Rails.env.development?
    has_attached_file :logo,
      :styles => {:thumb => "360x270#", :small=>"50x50#" },
      :path => ":rails_root/public/:attachment/:id/:style/:filename",
      :url => "/:attachment/:id/:style/:filename",
      :default_url => "/assets/logo.png"
 else
    has_attached_file :photo,
      :styles => {:thumb => "360x270#", :small=>"20x20#" },
      :default_url => "http://s3.amazonaws.com/#{ENV['BUCKET_NAME']}/photos/default/:style/missing.png"
  end
  validates_attachment_content_type :logo, :content_type => /\Aimage\/.*\Z/
end
