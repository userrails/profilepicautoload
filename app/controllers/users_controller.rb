class UsersController < ApplicationController
	def new
       @user = User.new
	end	

	def create
       @user = User.new(params[:user])
       if @user.save
         redirect_to users_path
       else
       	 render :action => :new
       end
       
	end	

	def show
      @user = User.find(params[:id])
	end	

  def index
     @users = User.find(:all)
  end	

  def update_photo
     @user = User.find(params[:id])  
     @user.update_attributes(params[:user])
     redirect_to user_path(@user)     
  end

end