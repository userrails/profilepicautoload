Psqlproj::Application.routes.draw do
  resources :homes
  resources :users do
    post :update_photo
  end
  

  root :to => 'homes#index'
end